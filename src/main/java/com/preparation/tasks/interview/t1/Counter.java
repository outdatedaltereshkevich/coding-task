package com.preparation.tasks.interview.t1;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Counter {
    private Stream<String> stream;
    private CounterType type;

    public Counter(Stream<String> stream, CounterType type) {
        this.stream = stream;
        this.type = type;
    }

    public Map<String, Long> calculateCounters() throws IOException {
        switch (type) {
            case STREAM_API:
                return calculateStreamAPI();
            default:
                return calculateLoop();
        }
    }

    private Map<String, Long> calculateStreamAPI() throws IOException {
        Map<String, Long> counter = new TreeMap<>();
        return stream.collect(Collectors.groupingBy(k -> k, ()-> counter, Collectors.counting()));
    }

    private Map<String, Long> calculateLoop() throws IOException {
        Map<String, Long> counter = new TreeMap<>();
        List<String> list = stream.collect(Collectors.toList());
        list.forEach(str -> {
            Long currentCounter = counter.getOrDefault(str, 0L);
            counter.put(str, currentCounter + 1);
        });
        return counter;
    }
}
