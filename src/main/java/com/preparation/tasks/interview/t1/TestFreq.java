package com.preparation.tasks.interview.t1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class TestFreq {
    public static void main(String...args) {
        calculateUsingLoop();
        calculateUsingStreamApi();
    }

    private static void calculateUsingStreamApi() {
        try {
            System.out.println("Stream API START");
            ClassLoader classLoader = TestFreq.class.getClassLoader();
            File file = new File(classLoader.getResource("test.txt").getFile());
            Stream<String> stream = Files.lines(Paths.get(file.getPath()));
            Counter c1 = new Counter(stream, CounterType.STREAM_API);
            Long beforeCalculation = System.currentTimeMillis();
            Map<String, Long> counterMapStreamApi = c1.calculateCounters();
            Long afterCalculation = System.currentTimeMillis();
            System.out.println("Calculation counters time(ms) = " + (afterCalculation - beforeCalculation));
            Map<String, Long> counterMapStreamApiSorted = new TreeMap<>(Comparator.comparing(counterMapStreamApi::get).reversed());
            counterMapStreamApiSorted.putAll(counterMapStreamApi);
            Long afterSorting = System.currentTimeMillis();
            System.out.println("Sorting time(ms) = " + (afterSorting - afterCalculation));
            counterMapStreamApiSorted.forEach((s, aLong) -> System.out.println(s + " - " + aLong));
            System.out.println("Stream API FINISH\n");
        } catch (IOException e) {
            System.out.println("Error reading file");
            e.printStackTrace();
        }
    }

    private static void calculateUsingLoop() {
        try {
            System.out.println("Loop START");
            ClassLoader classLoader = TestFreq.class.getClassLoader();
            File file = new File(classLoader.getResource("test.txt").getFile());
            Stream<String> stream = Files.lines(Paths.get(file.getPath()));
            Counter c2 = new Counter(stream, CounterType.LOOP);
            Long beforeCalculation = System.currentTimeMillis();
            Map<String, Long> counterMapLoop = c2.calculateCounters();
            Long afterCalculation = System.currentTimeMillis();
            System.out.println("Calculation counters time(ms) = " + (afterCalculation - beforeCalculation));
            Map<String, Long> comparatorLoopSorted = new TreeMap<>(Comparator.comparing(counterMapLoop::get).reversed());
            comparatorLoopSorted.putAll(counterMapLoop);
            Long afterSorting = System.currentTimeMillis();
            System.out.println("Sorting time(ms) = " + (afterSorting - afterCalculation));
            comparatorLoopSorted.forEach((s, aLong) -> System.out.println(s + " - " + aLong));
            System.out.println("Loop FINISH\n");
        } catch (IOException e) {
            System.out.println("Error reading file");
            e.printStackTrace();
        }
    }
}
